/**
 *
 * @file: index.js
 * @author: Pascal Peinecke
 *
*/
'use strict';

import express from 'express'; // Module providing simple routing and web features
import bodyParser from 'body-parser'; // Module providing parsing of json post data
import options from '../options.json'; // import global static options
import chalk from 'chalk'; // Module for prettier terminal log
import ip from 'ip';
const app = express(); // Initialize Express instance

app.use(bodyParser.json()); // Initialize bodyParser

app.listen(options.express_port); // Launch Express Server and listen on port ${express_port}
console.log(chalk.blue('express server listening on ' + ip.address() + ':' + JSON.stringify(options.express_port)));
app.use('/outputs', express.static('outputs')); // Select 'outputs' as a static directory for express
// require('./config/routes')(app); // include routes for the express server
import routes from './config/routes';
routes(app);
