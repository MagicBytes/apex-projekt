/**
 *
 * @file: connect.js
 * @author: Pascal Peinecke
 *
*/

'use strict';

import oracledb from 'oracledb' // Module providing database access for the oracle database
import chalk from 'chalk';
import dbConfig from './config/dbconfig.js'; // store the connection credentials in a seperate file

export default function() {

  /**
   * @name getConnection
   * @description Connect to the oracle database
   * @param {string} user - the username used for the connection
   * @param {string} password - the password used for the connection
   * @param {string} connectString - the connection string provided to connect to as specific database server
   */
  getConnection: oracledb.getConnection({ // execute the database connection handler
      user: dbConfig.user, // specify the username used for the database connection
      password: dbConfig.password, // specify the password used for the database connection
      connectString: dbConfig.connectString // specify the connection string containig the database server to connect to
    },
    function(error, connection) {
      if (error) {
        // handle connection error
        console.error(chalk.red(error.message));
        return reject(error);
      }

      if (connection.oracleServerVersion < 1201000200) { // check for the correct oracle database server version
        console.error(chalk.red('This program only works with Oracle Database 12.1.0.2 or greater'), connection); // handle incompatibility error
      } else {
        //module.exports = {connection : connection};   // Export the database connection as a global object
        console.log(chalk.green('Connection was successful!'));
        let connection = connection;
      }

    })


};
