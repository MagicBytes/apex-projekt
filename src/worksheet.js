/**
 *
 * @file: worksheet.js
 * @author: Pascal Peinecke
 *
*/

'use strict';

import chalk from 'chalk';
import Excel from 'exceljs'; // include exceljs for generation of Excel Files
import moment from 'moment-timezone'; // include the momentjs library to save the creation date of theExcel File

/**
 * @name generateWithFormat
 * @description Generate excel sheet from the parsed json array
 * @param {string} query - The query to be used.
 * @param {string} sheet_data - The json array which contains the cell data.
 * @param {string} sheet_meta - The json array which contains the cell headings.
 * @param {string} filelocation - The filename to which the excel workbook should be written to.
 * @return {Promise} Promise - return a promise to handle async io
 */
export function generateWithFormat(query, sheet_meta, sheet_data, filelocation) {

  console.log(chalk.blue('generating worksheet with formatting options...'));

  return new Promise((resolve, reject) => {
    // reject and resolve are functions provided by the Promise
    // implementation. Call only one of them.
    moment().tz("Europe/Berlin").format('DD.MM.YYYY'); // set the timezone for moment to Berlin
    console.log(chalk.blue('timezone set to Europe/Berlin'))

    let options = {
      useStyles: true // enable styling
    };

    let workbook = new Excel.Workbook(options);
    console.log(chalk.blue('workbook initialized'))

    workbook.creator = 'Node.JS Server'; // set the workbook creator to ${workbook.creator}

    let CurrentDay = moment().toDate().getDate();
    let CurrentMonth = moment().toDate().getMonth();
    let CurrentYear = moment().toDate().getFullYear();

    workbook.created = new Date(CurrentYear, CurrentMonth, CurrentDay); // set creation date
    let worksheet = workbook.addWorksheet('testsheet'); // add primary worksheet
    console.log(chalk.blue('worksheet added'))

    for (let i = 1; i <= sheet_meta.length; i++) {
      worksheet.getColumn(i).header = sheet_meta[i - 1].name; // assign column headers
    }

    console.log(chalk.blue('columns added'))

    worksheet.addRows(sheet_data); // add rows from query
    console.log(chalk.blue('rows added'))


    // === begin formatting options === //

    if (query.format.freezingHeader == true) {
      console.log(chalk.blue('Freezing Header enabled!'));
      worksheet.views = [{
        state: 'frozen',
        ySplit: 1
      }];
    }

    if (query.format.font.enabled == true) {
      if (query.format.font.all == true) {
        // Iterate over all rows that have values in a worksheet
        worksheet.eachRow(function(row, rowNumber) {
          console.log(chalk.blue('Applying font ' + query.format.font.name + ' for row ' + rowNumber));
          console.log(chalk.blue('Font Size: ' + query.format.font.size));
          console.log(chalk.blue('Underline: ' + query.format.font.underline));
          console.log(chalk.blue('Bold: ' + query.format.font.bold));
          worksheet.getRow(rowNumber).font = {
            name: query.format.font.name,
            family: query.format.font.family,
            size: query.format.font.size,
            underline: query.format.font.underline,
            bold: query.format.font.bold
          };

        });
      } else {
        console.log(chalk.blue('Applying font ' + query.format.font.name + ' for row ' + query.format.font.row));
        console.log(chalk.blue('Font Size: ' + query.format.font.size));
        console.log(chalk.blue('Underline: ' + query.format.font.underline));
        console.log(chalk.blue('Bold: ' + query.format.font.bold));
        worksheet.getRow(query.format.font.row).font = {
          name: query.format.font.name,
          family: query.format.font.family,
          size: query.format.font.size,
          underline: query.format.font.underline,
          bold: query.format.font.bold
        };
      }
    }

    if (query.format.tabColor.enabled == true) {
      console.log(chalk.blue('Tab Colors enabled!'));
      console.log(chalk.blue('Tab Color: ' + query.format.tabColor.color)); // has to be HEX code without the leading #
      worksheet.properties.tabColor = {
        argb: query.format.tabColor.color
      };
    }

    if (query.format.showGridLines == true) {
      console.log(chalk.blue('GridLines enabled!'));
      worksheet.pageSetup.showGridLines = true;
    } else {
      worksheet.pageSetup.showGridLines = false;
    }

    if (query.format.setOrientation.enabled == true) {
      console.log(chalk.blue('Orientation set to ' + query.format.setOrientation.orientation));
      worksheet.pageSetup.orientation = query.format.setOrientation.orientation;
    }

    // === end formatting options === //


    console.log(chalk.blue('schreibe datei in ' + filelocation));

    // finally write the excel file
    workbook.xlsx.writeFile(filelocation)
      .then(function(err) {
        if (err) {
          console.log(chalk.red('fehler traten beim Schreiben der Datei auf.'));
          console.log(chalk.red('Fehler:' + err));
          return reject(err);
        } else {
          console.log(chalk.green('datei erfolgreich geschrieben.'));
          return resolve(result);
        }
      });
  })

}
